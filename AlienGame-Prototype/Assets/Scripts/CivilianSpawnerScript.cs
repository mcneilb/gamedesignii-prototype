﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CivilianSpawnerScript : MonoBehaviour {

    public GameObject civilian;
    float randX;
    Vector2 whereToSpawn;
    public float spawnRate = 2f;
    float nextSpawn = 0.0f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (Time.time > nextSpawn)
        {
            nextSpawn = Time.time + spawnRate;
            randX = Random.Range(-15f, 15f);
            whereToSpawn = new Vector2(randX, transform.position.y);
            Instantiate(civilian, whereToSpawn, Quaternion.identity);
        }
	}
}
